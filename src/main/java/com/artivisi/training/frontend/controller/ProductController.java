package com.artivisi.training.frontend.controller;

import com.artivisi.training.frontend.service.CatalogBackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {

    @Autowired private CatalogBackendService catalogService;

    @GetMapping("/product/list")
    public ModelMap daftarProduct(){
        ModelMap mm = new ModelMap();
        mm.addAttribute("dataProduct", catalogService.dataProduct(PageRequest.of(0, 10, Sort.Direction.ASC, "kode")));
        return mm;
    }
}

