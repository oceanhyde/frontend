package com.artivisi.training.frontend.controller;


import com.artivisi.training.frontend.service.CatalogBackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BackendInfoController {
    @Autowired private CatalogBackendService catalogBackendService;
    @GetMapping("/backendinfo")
    public ModelMap showInfo(){
        ModelMap m = new ModelMap();

        m.addAttribute("informasiBackend", catalogBackendService.backendInfo());
        return m;
    }

}

