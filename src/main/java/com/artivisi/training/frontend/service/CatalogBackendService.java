package com.artivisi.training.frontend.service;

import com.artivisi.training.frontend.dto.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name = "gateway", fallback = CatalogFallbackService.class)
public interface CatalogBackendService {
    @GetMapping("/catalog/product/")
    Page<Product> dataProduct(Pageable page);

    @GetMapping ("/catalog/hostinfo")
    Map<String,Object> backendInfo();
}
