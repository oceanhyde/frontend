package com.artivisi.training.frontend.service;

import com.artivisi.training.frontend.dto.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CatalogFallbackService implements CatalogBackendService {

    @Override
    public Page<Product> dataProduct(Pageable page){
            return Page.empty(page);
    }
    @Override
    public Map<String, Object> backendInfo() {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", "UNKNOWN");
        info.put("address", "0.0.0.0");
        info.put("port", -1);
        return info;
    }
}
