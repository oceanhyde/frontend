package com.artivisi.training.frontend.dto;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;

}
